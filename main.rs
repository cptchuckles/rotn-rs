fn usage(ret: i32, examples: bool) -> !
{
	println!("Usage: rotn [OPTION]... [STRING]... -- [FILE]...");
	println!("Cyclically rotate alphabetic latin characters from files or string arguments.");
	println!("If FILE and STRING are unspecified, or FILE is -, read standard input");
	println!();
	println!("OPTIONS");
	println!("    -p, --print-offset    Prefix the output with the rotation offset");
	println!("    -n, --rotation <N>    Specify the rotation offset (default: 13)");
	println!("    -f, --file <path>     Rotate contents of a file specified by path");
	println!("    -h, --help            Print this message and exit (use long option for examples)");

	if examples {
		println!();
		println!("EXAMPLES");
		println!("    Rotate-13 on arguments:");
		println!("    $ rotn 'hello world'         # => uryyb jbeyq");
		println!();
		println!("    Rotate-13 on stdin:");
		println!("    $ echo hello world | rotn    # => uryyb jbeyq");
		println!();
		println!("    Rotate file contents by 9:");
		println!("    $ rotn -9 -- file.txt");
		println!();
		println!("    Rotate output of multiple commands by -9, or 17:");
		println!("    $ echo two | rotn -n -9 $(echo one) - -f<(echo three)");
		println!("    # => fev");
		println!("         knf");
		println!("         kyivv");
	}

	std::process::exit(ret);
}
macro_rules! usage {
	() => {
		usage(0, false)
	};
	($r: expr) => {
		usage($r, false)
	};
	($r: expr, $e: expr) => {
		usage($r, $e)
	};
}

enum Arg {
	Word(String),
	File(String),
}

fn rotate(input: &str, offset: usize) -> String
{
	input.chars()
	     .map(|c| match c {
	     	'a'..='z' => (((c as u8 - b'a') + offset as u8) % 26u8 + b'a') as char,
	     	'A'..='Z' => (((c as u8 - b'A') + offset as u8) % 26u8 + b'A') as char,
	     	_ => c,
	     })
	     .collect::<String>()
}

fn main() -> Result<(), &'static str>
{
	let mut argv = std::env::args();

	let mut print_offset = false;
	let mut rotation : Result<i32, _> = Ok(13);
	let mut get_rotation_arg = false;
	let mut get_file_arg = false;

	let mut args = Vec::<Arg>::new();

	let _ = argv.next(); //throw away program name
	while let Some(opt) = argv.next() {
		if get_rotation_arg {
			rotation = opt.parse::<i32>();
			get_rotation_arg = false;
		}
		else if get_file_arg {
			args.push(Arg::File(opt));
			get_file_arg = false;
		}
		else if opt.starts_with("--") {
			match opt.as_str() {
				"--print-offset" => print_offset = true,
				"--rotation" => get_rotation_arg = true,
				"--file" => get_file_arg = true,
				"--help" => usage!(0, true),
				"--" => {
					args.append(&mut argv.map(|a| Arg::File(a)).collect());
					break;
				},
				_ => {
					eprintln!("Unknown option: {}", opt);
					usage!(1);
				},
			}
		}
		else if opt.starts_with("-") {
			let opt = opt.split_at(1).1;
			if opt.len() == 0 {
				args.push(Arg::File(String::from("-")));
				continue;
			}
			let mut chars = opt.chars();
			while let Some(optc) = chars.next() {
				if optc.is_numeric() {
					rotation = opt.parse();
					break;
				}
				match optc {
					'p' => print_offset = true,
					'n' => {
						let rest = chars.collect::<String>();
						if rest.len() > 0 {
							rotation = rest.parse::<i32>();
						} else {
							get_rotation_arg = true;
						}
						break;
					},
					'f' => {
						let rest = chars.collect::<String>();
						if rest.len() > 0 {
							args.push(Arg::File(rest));
						} else {
							get_file_arg = true;
						}
						break;
					},
					'h' => usage!(),
					_ => {
						eprintln!("Unknown option: {}", optc);
						usage!(1);
					},
				}
			}
		}
		else {
			args.push(Arg::Word(opt));
		}
	}
	if get_rotation_arg || get_file_arg {
		usage!(1);
	}

	let rotation = rotation.or(Err("Invalid rotation argument"))?;
	let rotation = ((26 + rotation % 26) % 26) as usize;  // absolute modulo

	if args.is_empty() {
		args.push(Arg::File(String::from("-")));
	}
	let mut arg = args.iter();
	while let Some(a) = arg.next() {
		if print_offset {
			print!("{}: ", rotation);
		}
		match a {
			Arg::Word(w) => println!("{}", rotate(w, rotation)),
			Arg::File(f) => {
				let mut contents = String::new();
				let f = f.as_str();
				if f == "-" {
					use std::io::{Read, stdin};
					match stdin().read_to_string(&mut contents) {
						Err(e) => {
							eprintln!("Error: {}", e);
							std::process::exit(2);
						},
						_ => print!("{}", rotate(contents.as_str(), rotation)),
					}
					continue;
				}
				contents = match std::fs::read_to_string(f) {
					Ok(s) => s,
					Err(e) => {
						eprintln!("Error: {}: {}", f, e);
						std::process::exit(2);
					},
				};
				print!("{}", rotate(contents.as_str(), rotation));
			},
		}
	}

	Ok(())
}
