TARGET=build/rotn

clean:
	rm -f build/*

build: $(TARGET)
	mkdir -p build
	rustc -Oo build/rotn main.rs
	strip -s build/rotn

install:
	ln -sf "$(realpath build/rotn)" ~/.local/bin/rotn

all: clean build install

build/rotn: main.rs
